// https://github.com/michael-ciniawsky/postcss-load-config

module.exports = {
  //   parser: 'sugarss',
  plugins: {
    //'postcss-import': {},
    //'postcss-cssnext': {},
     'autoprefixer': {browsers: '> 1%, last 2 versions, Firefox ESR, Opera 12.1'},
     'cssnano': {
      // core:true,
      discardComments: false,// se desactivó para manipular comentarios para sass
      core: false // se desactivó para manipular outputStyle para sass
    },
    'postcss-pxtorem':{}
  }
}
