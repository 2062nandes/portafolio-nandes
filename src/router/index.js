import Vue from 'vue'
import Router from 'vue-router'
import Inicio from '@/components/Inicio'
import Curriculum from '@/components/Curriculum'
import Portafolio from '@/components/Portafolio'
import Noexiste from '@/components/404'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Inicio',
      component: Inicio
    },
    {
      path: '/curriculum',
      name: 'Curriculum',
      component: Curriculum
    },
    {
      path: '/portafolio',
      name: 'Portafolio',
      component: Portafolio
    },
    {
      path: '*',
      component: Noexiste
    }
  ]
})
